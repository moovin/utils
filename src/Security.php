<?php

namespace Moovin\Utils;

/**
 * Classe de seguran�a de dados, realiza a encriptar e decriptar baseado em uma chave secreta.
 *
 * @author Cassiano Erthal de Mesquita <cassiano.mesquita@moovin.com.br>
 */
class Security
{
    /** @var string Constante da chave secreta. */
    const DEFAULT_SECRET_KEY = '45045450t4545g50454807tr8070rt8t07r';

    /** @var integer Tamanho padr�o da chave de seguran�a */
    const DEFAULT_KEY_LENGHT = 32;

    /** @var string Chave secreta para realizar processo de encriptar ou decriptar. */
    private $secretKey;

    /**
     * @param string|null $secretKey
     */
    public function __construct($secretKey = null)
    {
        $this->secretKey = self::DEFAULT_SECRET_KEY;

        if (!empty($secretKey)) {
            $this->secretKey = $secretKey;
        }
    }

    /**
     * Realiza a encriptar utilizando uma chave secreta.
     *
     * @param string $value Texto para encriptar.
     *
     * @return string
     */
    public function encrypt($value)
    {
        $result = '';
        for ($i = 0; $i < strlen($value); $i++) {

            $char = substr($value, $i, 1);

            $keychar = substr($this->secretKey, ($i % strlen($this->secretKey)) - 1, 1);

            $char = chr(ord($char) + ord($keychar));

            $result .= $char;
        }

        return base64_encode($result);
    }

    /**
     * Realiza a decriptar do dado utilizando uma chave secreta.
     *
     * @param string $value Texto para decriptar.
     *
     * @return string
     */
    public function decrypt($value)
    {
        $result = '';
        $value = base64_decode($value);
        for ($i = 0; $i < strlen($value); $i++) {

            $char = substr($value, $i, 1);

            $keychar = substr($this->secretKey, ($i % strlen($this->secretKey)) - 1, 1);

            $char = chr(ord($char) - ord($keychar));

            $result .= $char;
        }

        return $result;
    }

    /**
     * Gera uma chave de seguran�a.
     *
     * O tamanho da chave deve ser de no m�nimo 16 caracteres, caso n�o seja
     * utilizar� o tamanho padr�o, 32 caracteres
     *
     * @return string
     */
    public function generateSecretKey()
    {
        $keyLength = self::DEFAULT_KEY_LENGHT;
        $secretKey = $this->encrypt(mt_rand($keyLength, $keyLength));

        if (function_exists('random_bytes')) {
            $secretKey = bin2hex(random_bytes($keyLength));

        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $secretKey = bin2hex(openssl_random_pseudo_bytes($keyLength));

        } elseif (function_exists('mcrypt_create_iv')) {
            $secretKey = bin2hex(mcrypt_create_iv($keyLength, MCRYPT_DEV_URANDOM));
        }

        return $secretKey;
    }
}