<?php

namespace Moovin\Utils;

/**
 * Lib "Object"
 * @author Kelvin Gluszszak Lopes <kelvin.lopes@moovin.com.br>
 */
abstract class Object
{
    /**
     * Retorna nome da classe
     * @param object $class
     * @return string
     */
    public static function getClassName($class)
    {
        if (is_object($class)) {
            $className = trim(get_class($class), "\\");
        } else {
            $className = $class;
        }

        if (preg_match("~^(.*)\\\\(.*)$~m", $className, $matches)) {
            return $matches[2];
        }

        return $className;
    }

    /**
     * Verifica se é possivel chamar o método da classe
     * @param object|string Instância ou FQN (Fully qualified name)
     * @param string $method
     * @return bool
     */
    public static function isCallable($object, $method)
    {
        return method_exists($object, $method) &&
            is_callable([$object, $method]);
    }

    /**
     * Retorna nome das classes dos parametros
     * @param object|string $class Nome ou instancia do objeto. Tambem pode receber uma instancia de \ReflectionClass para melhor performance
     * @param string $method
     * @return array
     */
    public static function getMethodParamsClassName($class, $methodName)
    {
        if ($class instanceof \ReflectionClass) {
            $ReflectionClass = $class;
        } else {
            $ReflectionClass = new \ReflectionClass($class);
        }

        $Method = $ReflectionClass->getMethod($methodName);
        if (!$Method) {
            return null;
        }
        $parameters = $Method->getParameters();
        $return = [];
        foreach ($parameters as $Parameter) {
            if (!$Parameter->getClass()) {
                continue;
            }
            $return[] = $Parameter->getClass()->getName();
        }

        return $return;
    }

    /**
     * Transforma objeto para array
     * @param object $object
     * @return array
     */
    public static function toArray($object)
    {
        $return = [];

        foreach ($object as $key => $value) {
            if (is_object($value)) {
                if (self::isCallable($value, "toArray")) {
                    $value = $value->toArray();
                } elseif ($value instanceof \stdClass) {
                    $value = self::toArray($value);
                }
            }

            $return[$key] = $value;
        }

        return $return;
    }
}
