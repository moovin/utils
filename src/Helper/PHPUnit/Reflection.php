<?php
namespace Moovin\Utils\Helper\PHPUnit;

/**
 * Lib de Reflex�o para ajudar com o PHPUnit
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class Reflection
{
    /**
     * Obt�m uma reflex�o do m�todo da classe
     *
     * @param object|string $class Inst�ncia ou nome completo da classe
     *
     * @param string $name Nome da propriedade
     *
     * @return \ReflectionMethod
     */
    public static function getMethod($class, $name)
    {
        $method = (new \ReflectionClass($class))->getMethod($name);

        $method->setAccessible(true);

        return $method;
    }

    /**
     * Obt�m uma reflex�o da propriedade da classe
     *
     * @param object|string $class Inst�ncia ou nome completo da classe
     *
     * @param string $name Nome da propriedade
     *
     * @return \ReflectionProperty
     */
    public static function getProperty($class, $name)
    {
        $property = (new \ReflectionClass($class))->getProperty($name);

        $property->setAccessible(true);

        return $property;
    }
}
