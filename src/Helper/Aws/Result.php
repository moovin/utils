<?php

namespace Moovin\Utils\Helper\Aws;

use Aws\Result as AwsResult;

/**
 * Lib de ajuda com o Aws\Result
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class Result
{
    /**
     * Verifica se o resultado está na categoria http dos 2xx
     *
     * @param AwsResult $result
     *
     * @return boolean
     */
    public static function verifyOk(AwsResult $result)
    {
        $metadata = $result->get("@metadata");

        if (!$metadata || !isset($metadata["statusCode"])) {
            return false;
        }

        $statusCode = $metadata["statusCode"];

        return (0 | $statusCode / 100) === 2;
    }

    /**
     * Obtém o dados de retorno "efeitivos" da requisição
     *
     * @param AwsResult $result
     *
     * @return string
     */
    public static function getPayload(AwsResult $result)
    {
        return (string) $result->get("Payload");
    }

    /**
     * Obtém o corpo da resposta
     *
     * @param AwsResult $result
     *
     * @return string
     */
    public static function getBody(AwsResult $result)
    {
        return (string) $result->get("Body");
    }
}
