<?php

namespace Moovin\Utils;

/**
 * Classe para operações com números.
 *
 * @author Leonardo Oliveira <leonardo.malia@moovin.com.br>
 */
class Number
{
    /**
     * Converte dinheiro (mascara da view) para float.
     *
     * @param string $money
     *
     * @return float
     */
    public static function formatMoneyToFloat($money)
    {
        $money = (string) str_replace(['.', ',', ' '], '', $money);
        return (float) ($money / 100);
    }
}