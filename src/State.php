<?php
namespace Moovin\Utils;

/**
 * Opera��es de convers�es/manipula��es de estados.
 *
 * @author Gabriel Anhaia <gabriel.silva@moovin.com.br>
 */
class State
{
    /** @var array $stateInitials Mapa de siglas de estados. */
    protected static $stateInitials = [
        "AC" => "acre",
        "AL" => "alagoas",
        "AM" => "amazonas",
        "AP" => "amapa",
        "BA" => "bahia",
        "CE" => "ceara",
        "DF" => "distrito federal",
        "ES" => "espirito santo",
        "GO" => "goias",
        "MA" => "maranhao",
        "MT" => "mato grosso",
        "MS" => "mato grosso do sul",
        "MG" => "minas gerais",
        "PA" => "para",
        "PB" => "paraiba",
        "PR" => "parana",
        "PE" => "pernambuco",
        "PI" => "piaui",
        "RJ" => "rio de janeiro",
        "RN" => "rio grande do norte",
        "RO" => "rondonia",
        "RS" => "rio grande do sul",
        "RR" => "roraima",
        "SC" => "santa catarina",
        "SE" => "sergipe",
        "SP" => "sao paulo",
        "TO" => "tocantins"
    ];

    /**
     * Retorna a sigla de um estado atrav�s de seu nome.
     *
     * @param string $fullNameState Nome Completo do estado.
     *
     * @return bool|int|mixed|string
     */
    public static function getInitialsState($fullNameState)
    {
        $fullNameState = Text::noSpecialCharacters(strtolower($fullNameState));

        return array_search($fullNameState, self::$stateInitials);
    }
}