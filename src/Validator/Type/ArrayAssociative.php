<?php
namespace Moovin\Utils\Validator\Type;

/**
 * Validar campos de acordo com os m�todos est�ticos, com objetivo de n�o
 * precisar de inst�ncias para tal. E agrupar fun��es para todos utilizarem
 * as mesmas fun��es de valida��o.
 *
 * @author Ernando Souza <ernando.silva@moovin.com.br>.
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>.
 */
class ArrayAssociative
{
    /**
     * Valida se os campos obrigat�rios s�o nulos, indefinidos, falso, ou string vazia.
     *
     * @param array $required Array com os campos a serem validados, no formato,
     *                          "codigo_produto" => "codigo do produto", a chave
     *                          deve ser a mesma que � armazenada no
     *                          data_to_validate, e o valor deve ser o valor de
     *                          apresenta��o.
     *                          que poder� ser utilizado em uma mensagem de erro.
     * @param array $data_to_validate Array com os dados que devem ser validados,
     *                                  no formato, "codigo_produto" => "51264"
     *
     * @return array Array com os campos que est�o inv�lidos.
     */
    public static function requiredFields(array $required, array $data_to_validate)
    {
        $missing = array_diff_key($required, array_filter($data_to_validate, function ($value) {
            return $value !== false && $value !== null && $value !== '';
        }));

        return $missing;
    }

    /**
     * Valida se o tamanho dos campos s�o menores do que o tamanho m�ximo.
     *
     * @param array $length_permitted Array com os campos a serem validados,
     *                                no formato, "nome" => 240.
     * @param array $data_to_validate Array com os dados que devem ser validados,
     *                                  no formato, "nome" => "jo�o".
     *
     * @return boolean True se todos campos foram validados, e false se algum
     *                   campo n�o foi validado.
     */
    public static function fieldLength(array $length_permitted, array $data_to_validate)
    {
        foreach (array_intersect_key($data_to_validate, $length_permitted) as $fieldname => $field) {
            if (strlen($field) > $length_permitted[$fieldname]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Valida se o tamanho dos campos s�o menores do que o tamanho m�ximo.
     *
     * @param array $validation Array com os campos a serem validados,
     *                            no formato, "nome" => 240.
     * @param array $toValidate Array com os dados que devem ser
     *                            validados, no formato, "nome" => "jo�o".
     * @param array $presentation Array com a chave contendo o nome do campo
     *                              e o valor contendo um nome de apresenta��o
     *                              do campo, no formato,
     *                              "codigo_produto" => "c�digo do produto".
     *
     * @return array Array com os campos que est�o inv�lidos.
     */
    public static function fieldLengthMax($validation, $toValidate, $presentation = [])
    {
        $field_invalidated = [];
        foreach (array_intersect_key($toValidate, $validation) as $fieldname => $field) {
            if (strlen($field) > $validation[$fieldname]) {
                $field_invalidated[] = ($presentation[$fieldname]) ? $presentation[$fieldname] : $fieldname;
            }
        }

        return $field_invalidated;
    }

    /**
     * Valida se os campos s�o n�meros maiores que zero (exclusivamente)
     *
     * @param array $numbers Array com os campos a serem validados, no formato,
     *                       "qtd" => "quantidade", a chave deve ser a mesma que
     *                       � armazenada no toValidate, e o valor deve ser o
     *                       valor de apresenta��o que poder� ser utilizado em
     *                       uma mensagem de erro.
     * @param array $toValidate Array com os dados que devem ser validados,
     *                                no formato, "codigo_produto" => "51264"
     *
     * @return array Array com os campos que est�o inv�lidos.
     */
    public static function positiveNumberField(array $numbers, array $toValidate)
    {
        $field_invalidated = array_diff_key($numbers, array_filter($toValidate, function ($value) {
            return $value > 0;
        }));

        return $field_invalidated;
    }

    /**
     * Verifica e retorna true se valor recebido for um JSON v�lido.
     * Retorna false caso contr�rio.
     *
     * @todo implementar novo m�todo que retorna o tipo de erro que ocorreu
     *       ou o valor encodado, caso v�lido.
     *
     * @param mixed $value
     *
     * @return bool Verdadeiro se � um json v�lido, caso contr�rio, falso.
     */
    public static function isValidJson($value)
    {
        json_decode($value);

        if (json_last_error() === JSON_ERROR_NONE) {
            return true;
        }

        return false;
    }
}