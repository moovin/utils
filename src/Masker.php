<?php
namespace Moovin\Utils;

/**
 * Lib para trabalhar com mascaras
 * @see http://ninguemfez.blogspot.com.br/2014/05/php-mascara-php-mask.html
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class Masker
{
    /** @var string CPF_MASK Mascara de CPF */
    const CPF_MASK = "%%%.%%%.%%%-%%";

    /** @var string CNPJ_MASK Mascara de CNPJ */
    const CNPJ_MASK = "%%.%%%.%%%/%%%%-%%";

    /** @var string CNPJ_MASK Mascara de CNPJ */
    const CEP_MASK = "%%%%%%-%%%";

    /** @var string DATE_MASK Mascara de Data padr�o Brasileiro */
    const DATE_MASK = "%%/%%/%%%%";

    /**
     * Aplica a mascara definida ao valor informado.
     * @param string $mask
     * @param mixed $value
     * @return string
     */
    public static function apply($mask, $value)
    {
        $mask = (string) $mask;

        $value = (string) $value;

        $masked = "";

        $maskSize = strlen($mask) - 1;

        for ($maskPointer = 0, $valuePointer = 0; $maskPointer <= $maskSize; $maskPointer++) {
            switch ($mask[$maskPointer]) {

                case "#":
                    if (!isset($value[$valuePointer])) {
                        break;
                    }

                    $masked .= $value[$valuePointer++];

                    break;

                case "%":
                    if (!isset($value[$valuePointer])) {
                        break;
                    }

                    $value[$valuePointer] = (integer) $value[$valuePointer];

                    $masked .= $value[$valuePointer++];

                    break;

                case "\\":
                    $maskPointer++;

                default:
                    $masked .= $mask[$maskPointer];
            }
        }

        return $masked;
    }

    /**
     * Retorna vazio quando a mascara ficar em "branco".
     * @param string $mask
     * @param string $masked
     * @return string
     */
    public static function cleanMaskOnEmpty($mask, $masked)
    {
        $emptyMasked = self::apply($mask, "");

        if ($masked === $emptyMasked) {
            $masked = "";
        }

        return $masked;
    }

    /**
     * M�todo m�gico para aplicar o tipo de mascara baseado na constante.
     * E tamb�m adicionalmente pode fazer a��es extras baseadas no sufixos
     * adicionados
     *<code>
     *  Masker::cpf("89765256043"); // "897.652.560-43"
     *  Masker::cpf(""); // "..-"
     *  Masker::cpfOrEmpty(""); // ""
     *</code>
     * @param string $name nome chamado
     * @param array $arguments par�metros passados
     * @throws \Exception
     * @return string
     */
    public static function __callStatic($name, $arguments)
    {
        $sufixes = [
            "OrEmpty" => [
                "exists" => false,
                "callback" => function ($masked) use (&$maskValue) {
                    return self::cleanMaskOnEmpty($maskValue, $masked);
                }
            ]
        ];

        $mask = $name;

        foreach ($sufixes as $sufix => &$options) {
            $mask = \preg_replace("~{$sufix}$~", "", $mask, -1, $options["exists"]);
        }

        $maskConstant = __CLASS__ . "::" . \strtoupper($mask) . "_MASK";

        if (!defined($maskConstant)) {
            throw new \Exception("Mask $mask doesn't exists");
        }

        $maskValue = \constant($maskConstant);

        $masked = self::apply($maskValue, $arguments[0]);

        foreach ($sufixes as $sufix => &$options) {
            if (!$options["exists"]) {
                continue;
            }

            $masked = \call_user_func($options["callback"], $masked);
        }

        return $masked;
    }
}
