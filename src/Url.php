<?php

namespace Moovin\Utils;

/**
 * Opera��es com URL.
 *
 * @author Leonardo Oliveira <leonardo.malia@moovin.com.br>
 */
class Url
{
    /**
     * Retorna uma rota completa com o dom�nio do cliente concatenado com o par�metro $url
     *
     * @param string $url
     *
     * @return string
     */
    public static function getRoute($url)
    {
        if ($_SERVER['SERVER_NAME'] == 'servidor') {
            $uriSegments = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

            $route = array_slice($uriSegments, 0, 2);
            $route[] = $url;

            array_unshift($route, $_SERVER['HTTP_HOST']);

            return implode('/', $route);
        }

        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/' . $url;
    }

    /**
     * Retorna o nome do site de acordo com a URL.
     *
     * @return string
     */
    public static function getSiteName()
    {
        $url = strstr($_SERVER['SERVER_NAME'], '.com', true);

        if (strpos($url, 'www.') !== false && strpos($url, 'moovinplataforma') === false) {
            return str_replace('www.', '', $url);
        }

        if ($url == 'www') {
            return strstr($_SERVER['SERVER_NAME'], '.');
        }

        $moovinEnvironments = [
            'vcs' => 'moovin.vcs.moovinplataforma',
            'testing' => 'testing.moovinplataforma',
            'sandbox' => 'sandbox.moovinplataforma'
        ];

        foreach ($moovinEnvironments as $envName => $envUrl) {
            if (strpos($_SERVER['SERVER_NAME'], $envUrl)) {
                $sufix = $envName == 'vcs' ? '' :  '_' . $envName;

                $uri = array_filter(explode('/', $_SERVER['REQUEST_URI']));

                $validUri = $uri[1] == 'v2' ? $uri[2] : $uri[1];

                return $validUri . $sufix;
            }
        }

        if (strpos($_SERVER['SERVER_NAME'], 'homologacao') !== false) {
            return explode('/', $_SERVER['REQUEST_URI'])[1];
        }

        if ($_SERVER['SERVER_NAME'] == 'servidor') {
            $uriSegments = explode('/', $_SERVER['REQUEST_URI']);

            if ($uriSegments[1] == 'sites') {
                return str_replace('site-', '', $uriSegments[2]) . '_debug';
            }

            if ($uriSegments[1] == 'repositorios') {
                return str_replace('site-', '', $uriSegments[3]) . '_debug';
            }

            return $uriSegments[1] . '_debug';
        }

        if (strpos($_SERVER['SERVER_NAME'], 'staging.moovin.com.br') !== false && $_SERVER['SITE_REPOSITORY']) {
            return str_replace('site-', '', $_SERVER['SITE_REPOSITORY']);
        }

        return str_replace(['www.', '.com.br', '.com'], '', $_SERVER['SERVER_NAME']);
    }
}