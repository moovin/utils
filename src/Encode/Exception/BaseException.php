<?php

namespace Moovin\Utils\Encode\Exception;

/**
 * Classe abstrata para exceções
 *
 * @package Moovin\Utils\Encode\Exception
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class BaseException extends \Exception
{
}
