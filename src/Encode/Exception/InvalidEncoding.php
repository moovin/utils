<?php

namespace Moovin\Utils\Encode\Exception;

/**
 * Classe para exceção de codificação inválida
 *
 * @package Moovin\Utils\Encode\Exception
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class InvalidEncoding extends BaseException
{
}
