<?php

namespace Moovin\Utils\Encode\Exception;

/**
 * Classe para exceção de classe de tipo inválido
 *
 * @package Moovin\Utils\Encode\Exception
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class InvalidTypeClass extends BaseException
{
}
