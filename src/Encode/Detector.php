<?php

namespace Moovin\Utils\Encode;

/**
 * Classe de deteção de codificação de textos
 *
 * @package Moovin\Utils\Encode
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class Detector
{
    /**
     * @var string Lista de classes do pacote que implementam o contrato de tipo
     * de codificação
     */
    const TYPE_CLASS_LIST = [
        Type\Ascii::class,
        Type\Utf8::class
    ];

    /**
     * Obtém o nome da codificação da informação passada
     *
     * @param string $data Informação que se deseja verificar
     *
     * @throws Exception\InvalidEncoding Caso a informação passada não seja de
     * nenhum tipo
     *
     * @return string
     */
    public static function getEncodingName($data)
    {
        $type = static::oneOf(static::TYPE_CLASS_LIST, $data);
        
        return $type::getName();
    }

    /**
     * Verifica se a informação é de algum dos tipos informados
     *
     * @param Contract\Type[] $typeList Lista de tipos para serem verificados
     *
     * @param string $data Informação que se deseja verificar
     *
     * @throws Exception\InvalidTypeClass Caso alguma classe não implemente o
     * contrato de tipo de codificação
     *
     * @throws Exception\InvalidEncoding Caso a informação passada não seja de
     * nenhum tipo dos especificados
     *
     * @return Contract\Type
     */
    public static function oneOf(array $typeList, $data)
    {
        foreach ($typeList as $type) {
            if (!static::classImplementsTypeContract($type)) {
                throw new Exception\InvalidTypeClass;
            }

            if ($type::validate($data)) {
                return $type;
            }
        }

        throw new Exception\InvalidEncoding;
    }
    
    /**
     * Verifica se a classe passada implementa o contrato de tipo de codificação
     *
     * @param mixed $class
     *
     * @return bool
     */
    public static function classImplementsTypeContract($class)
    {
        return is_subclass_of($class, Contract\Type::class);
    }
}
