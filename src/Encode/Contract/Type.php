<?php

namespace Moovin\Utils\Encode\Contract;

/**
 * Inteface para deteção e validação de codificações
 *
 * @package Moovin\Utils\Encode\Contract
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
interface Type
{
    /**
     * Obtém o nome da codificação
     *
     * @return string
     */
    public static function getName();

    /**
     * Verifica se contém a codificação
     *
     * @param string $data Informação a ser detectada
     *
     * @return bool
     */
    public static function detect($data);

    /**
     * Verifica se contém apenas a codificação
     *
     * @param string $data Informação a ser validada
     *
     * @return bool
     */
    public static function validate($data);
}
