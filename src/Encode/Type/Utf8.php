<?php

namespace Moovin\Utils\Encode\Type;

use Moovin\Utils\Encode\Contract\Type as TypeInterface;

/**
 * Classe de deteção e validação da codificação UTF-8
 *
 * @package Moovin\Utils\Encode\Type
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class Utf8 implements TypeInterface
{
    /**
     * @var string
     */
    const NAME = "UTF-8";

    /**
     * @var string
     */
    const DETECT_PATTERN = "~[\xC2-\xDF][\x80-\xBF]|
        \xE0[\xA0-\xBF][\x80-\xBF]|
        [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}|
        \xED[\x80-\x9F][\x80-\xBF]|
        \xF0[\x90-\xBF][\x80-\xBF]{2}|
        [\xF1-\xF3][\x80-\xBF]{3}|
        \xF4[\x80-\x8F][\x80-\xBF]{2}~s";

    /**
     * @var string
     */
    const VALIDATE_PATTERN = "~~u";

    /**
     * @inheritDoc
     */
    public static function getName()
    {
        return static::NAME;
    }

    /**
     * @inheritDoc
     */
    public static function detect($data)
    {
        return (bool) preg_match(static::DETECT_PATTERN, $data);
    }

    /**
     * @inheritDoc
     */
    public static function validate($data)
    {
        return (bool) preg_match(static::VALIDATE_PATTERN, $data);
    }
}
