<?php

namespace Moovin\Utils\Encode\Type;

use Moovin\Utils\Encode\Contract\Type as TypeInterface;

/**
 * Classe de deteção e validação da codificação ASCII
 *
 * @package Moovin\Utils\Encode\Type
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class Ascii implements TypeInterface
{
    /**
     * @var string
     */
    const NAME = "ASCII";

    /**
     * @var string
     */
    const DETECT_PATTERN = "~[\\x01-\\x7F]~";

    /**
     * @var string
     */
    const NOT_VALIDATE_PATTERN = "~[\\x80-\\xFF]~";

    /**
     * @inheritDoc
     */
    public static function getName()
    {
        return static::NAME;
    }

    /**
     * @inheritDoc
     */
    public static function detect($data)
    {
        return (bool) preg_match(static::DETECT_PATTERN, $data);
    }

    /**
     * @inheritDoc
     */
    public static function validate($data)
    {
        return !preg_match(static::NOT_VALIDATE_PATTERN, $data);
    }
}
