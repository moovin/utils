<?php
namespace Moovin\Utils\Encoder\Exception;

use Exception;

/**
 * Trata de exce��es lan�adas durante a codifica��o de Utf8.
 *
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
class Utf8Exception extends Exception
{

}