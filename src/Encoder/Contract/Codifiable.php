<?php
namespace Moovin\Utils\Encoder\Contract;

/**
 * Interface com m�todos que o codificador deve implementar.
 * M�todos para codificar (encode) e decodificar (decode).
 *
 * @author Matheus Gonzaga <matheus.gonzaga@moovin.com.br>
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
interface Codifiable
{
    /**
     * Converte pra outro charset a string recebida pelo par�metro
     * do tipo do codificador
     *
     * @param mixed $data Dados a serem decodificados do tipo do codificador
     *
     * @return mixed Dados decodificados do tipo do codificador
     */
    public static function decode($data);

    /**
     * Converte para o tipo do codificador os dados recebidos pelo par�metro
     *
     * @param mixed $data Dados a serem codificados para o tipo do codificador
     *
     * @return mixed Dados codificados para o tipo do codificador
     */
    public static function encode($data);
}
