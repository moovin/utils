<?php
namespace Moovin\Utils\Converter\Exception;

use Exception;

/**
 * Trata de exce��es lan�adas durante a convers�o de tipos para dados inv�lidos
 *
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
class InvalidTypeException extends Exception
{

}