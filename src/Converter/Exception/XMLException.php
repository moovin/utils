<?php
namespace Moovin\Utils\Converter\Exception;

use Exception;

/**
 * Trata de exceções lançadas durante a conversão dos tipos para xml e vice versa.
 *
 * @author Davi Alves <davi.alves@moovin.com.br>
 */
class XMLException extends Exception
{
    /** @var mixed $data Dados da tentativa de conversão. */
    protected $data;

    /**
     * Retorna os dados da tentativa de conversão.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Define os dados da tentativa de conversão.
     *
     * @param mixed $data
     *
     * @return XMLException
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}
