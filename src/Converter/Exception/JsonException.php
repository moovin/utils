<?php
namespace Moovin\Utils\Converter\Exception;

use Exception;

/**
 * Trata de exce��es lan�adas durante a convers�o dos tipos para json e vice versa.
 *
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 * @author Gabriel Anhaia <gabriel.silva@moovin.com.br>
 */
class JsonException extends Exception
{
    /** @var mixed $data Dados da tentativa de convers�o. */
    protected $data;

    /**
     * Retorna os dados da tentativa de convers�o.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Define os dados da tentativa de convers�o.
     *
     * @param mixed $data
     *
     * @return JsonException
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}