<?php
namespace Moovin\Utils\Converter\Type;

use DOMDocument;
use DOMNode;
use DOMText;
use Exception;
use Moovin\Utils\Converter\Exception\XMLException;
use Moovin\Utils\Encoder\Type\Utf8;
use SimpleXMLElement;
use Moovin\Utils\Converter\Contract\ConvertableArray;

/**
 * Trata das conversões de XML para array ou vice versa
 *
 * @author Dantiéris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
class Xml implements ConvertableArray
{
    /**
     * @param string|array $input XML string, a path to a file, a URL or an array
     * @param string|array $options The options to use
     *
     * @return \SimpleXMLElement|\DOMDocument SimpleXMLElement or DOMDocument
     * @throws \Exception
     */
    public static function build($input, array $options = [])
    {
        $defaults = [
            'return'       => 'simplexml',
            'loadEntities' => false,
            'readFile'     => true,
        ];
        $options += $defaults;

        if (is_array($input) || is_object($input)) {
            return static::fromArray($input, $options);
        }

        if (strpos($input, '<') !== false) {
            return static::parseXML($input, $options);
        }

        if ($options['readFile'] && file_exists($input)) {
            return static::parseXML(file_get_contents($input), $options);
        }

        if (!is_string($input)) {
            throw new \Exception('Invalid input.');
        }

        throw new \Exception('XML cannot be read.');
    }

    /**
     * Parse the input data and create either a SimpleXmlElement object or a DOMDocument.
     *
     * @param string $input The input to load.
     * @param array $options The options to use. See Xml::build()
     *
     * @return \SimpleXmlElement|\DOMDocument
     * @throws \Exception
     */
    protected static function parseXML($input, $options)
    {
        $hasDisable = function_exists('libxml_disable_entity_loader');
        $internalErrors = libxml_use_internal_errors(true);
        if ($hasDisable && !$options['loadEntities']) {
            libxml_disable_entity_loader(true);
        }
        try {
            if ($options['return'] === 'simplexml' || $options['return'] === 'simplexmlelement') {
                $xml = new SimpleXMLElement($input, LIBXML_NOCDATA | LIBXML_PARSEHUGE);
            } else {
                $xml = new DOMDocument();
                $xml->loadXML($input);
            }
        } catch (Exception $e) {
            $xml = null;
        }
        if ($hasDisable && !$options['loadEntities']) {
            libxml_disable_entity_loader(false);
        }
        libxml_use_internal_errors($internalErrors);
        if ($xml === null) {
            throw new \Exception('Xml cannot be read.');
        }

        return $xml;
    }

    /**
     * Transform an array into a SimpleXMLElement
     *
     * @param array $input Array with data or a collection instance.
     *
     * @return \SimpleXMLElement|\DOMDocument SimpleXMLElement or DOMDocument
     * @throws \Exception
     */
    public static function fromArray($input, $return = 'simplexml')
    {
        $defaults = [
            'format'   => 'tags',
            'version'  => '1.0',
            'encoding' => mb_internal_encoding(),
            'return'   => $return,
            'pretty'   => false,
        ];

        if (method_exists($input, 'toArray')) {
            $input = $input->toArray();
        }

        if (!is_array($input) || count($input) !== 1) {
            throw new \Exception('Invalid input.');
        }
        $key = key($input);
        if (is_int($key)) {
            throw new \Exception('The key of input must be alphanumeric');
        }

        $dom = new DOMDocument($defaults['version'], $defaults['encoding']);
        if ($defaults['pretty']) {
            $dom->formatOutput = true;
        }
        self::createChildren($dom, $dom, $input, $defaults['format']);

        $defaults['return'] = strtolower($defaults['return']);
        if ($defaults['return'] === 'simplexml' || $defaults['return'] === 'simplexmlelement') {
            return new SimpleXMLElement($dom->saveXML());
        }

        return $dom;
    }

    /**
     * Recursive method to create childs from array
     *
     * @param \DOMDocument $dom Handler to DOMDocument
     * @param \DOMElement $node Handler to DOMElement (child)
     * @param array $data Array of data to append to the $node.
     * @param string $format Either 'attributes' or 'tags'. This determines where nested keys go.
     *
     * @return void
     * @throws \Exception
     */
    protected static function createChildren($dom, $node, &$data, $format)
    {
        if (empty($data) || !is_array($data)) {
            return;
        }
        foreach ($data as $key => $value) {
            if (is_string($key)) {
                if (method_exists($value, 'toArray')) {
                    $value = $value->toArray();
                }

                if (!is_array($value)) {
                    if (is_bool($value)) {
                        $value = (int)$value;
                    } elseif ($value === null) {
                        $value = '';
                    }
                    if ($key[0] !== '@' && $format === 'tags') {
                        if (!is_numeric($value)) {
                            // Escape special characters
                            // http://www.w3.org/TR/REC-xml/#syntax
                            // https://bugs.php.net/bug.php?id=36795
                            $child = $dom->createElement($key, '');
                            $child->appendChild(new DOMText($value));
                        } else {
                            $child = $dom->createElement($key, $value);
                        }
                        $node->appendChild($child);
                    } else {
                        if ($key[0] === '@') {
                            $key = substr($key, 1);
                        }
                        $attribute = $dom->createAttribute($key);
                        $attribute->appendChild($dom->createTextNode($value));
                        $node->appendChild($attribute);
                    }
                } else {
                    if ($key[0] === '@') {
                        throw new \Exception('Invalid array');
                    }
                    if (is_numeric(implode('', array_keys($value)))) {
                        // List
                        foreach ($value as $item) {
                            $itemData = compact('dom', 'node', 'key', 'format');
                            $itemData['value'] = $item;
                            static::createChild($itemData);
                        }
                    } else {
                        // Struct
                        static::createChild(compact('dom', 'node', 'key', 'value', 'format'));
                    }
                }
            } else {
                throw new \Exception('Invalid array');
            }
        }
    }

    /**
     * Helper to createChildren(). It will create childs of arrays
     *
     * @param array $data Array with informations to create childs
     *
     * @return void
     */
    protected static function createChild($data)
    {
        extract($data);
        $childNS = $childValue = null;
        /** @var mixed $value */
        if (method_exists($value, 'toArray')) {
            $value = $value->toArray();
        }
        if (is_array($value)) {
            if (isset($value['@'])) {
                $childValue = (string)$value['@'];
                unset($value['@']);
            }
            if (isset($value['xmlns:'])) {
                $childNS = $value['xmlns:'];
                unset($value['xmlns:']);
            }
        } elseif (!empty($value) || $value === 0) {
            $childValue = (string)$value;
        }

        /** @var mixed $dom */
        /** @var mixed $key */
        $child = $dom->createElement($key);
        if ($childValue !== null) {
            $child->appendChild($dom->createTextNode($childValue));
        }
        if ($childNS) {
            $child->setAttribute('xmlns', $childNS);
        }

        /** @var mixed $format */
        static::createChildren($dom, $child, $value, $format);
        /** @var mixed $node */
        $node->appendChild($child);
    }

    /**
     * Returns this XML structure as an array.
     *
     * @param \SimpleXMLElement|\DOMDocument|\DOMNode $data SimpleXMLElement, DOMDocument or DOMNode instance
     *
     * @return array Array representation of the XML structure.
     * @throws \Exception
     */
    public static function toArray($data)
    {
        if (is_string($data) && strpos(trim($data, "\t\n"), '<') === 0) {
            $options = [
                'return'       => 'simplexml',
                'loadEntities' => false,
                'readFile'     => true,
            ];

            $data = static::parseXML(Utf8::encode($data), $options);
        }

        if ($data instanceof DOMNode) {
            $data = simplexml_import_dom($data);
        }

        if (!($data instanceof SimpleXMLElement)) {
            $exception = new XMLException('The input is not instance of SimpleXMLElement, DOMDocument or DOMNode.');
            $exception->setData($data);
            throw $exception;
        }

        $result = [];
        $namespaces = array_merge(['' => ''], $data->getNamespaces(true));
        static::toArrayRecursive($data, $result, '', array_keys($namespaces));

        return $result;
    }

    /**
     * Recursive method to toArray
     *
     * @param \SimpleXMLElement $xml SimpleXMLElement object
     * @param array $parentData Parent array with data
     * @param string $ns Namespace of current child
     * @param array $namespaces List of namespaces in XML
     *
     * @return void
     */
    protected static function toArrayRecursive($xml, &$parentData, $ns, $namespaces)
    {
        $data = [];

        foreach ($namespaces as $namespace) {
            foreach ($xml->attributes($namespace, true) as $key => $value) {
                if (!empty($namespace)) {
                    $key = $namespace . ':' . $key;
                }
                $data['@' . $key] = (string)$value;
            }

            foreach ($xml->children($namespace, true) as $child) {
                static::toArrayRecursive($child, $data, $namespace, $namespaces);
            }
        }

        $asString = trim((string)$xml);
        if (empty($data)) {
            $data = $asString;
        } elseif (strlen($asString) > 0) {
            $data['@'] = $asString;
        }

        if (!empty($ns)) {
            $ns .= ':';
        }
        $name = $ns . $xml->getName();
        if (isset($parentData[$name])) {
            if (!is_array($parentData[$name]) || !isset($parentData[$name][0])) {
                $parentData[$name] = [$parentData[$name]];
            }
            $parentData[$name][] = $data;
        } else {
            $parentData[$name] = $data;
        }
    }
}
