<?php

namespace Moovin\Utils\Converter\Type;

use Moovin\Utils\Converter\Contract\ConvertableArray;

/**
 * Trata das convers�es de CSV para array ou vice versa.
 *
 * @author Thiago Hofmeister <thiago.souza@moovin.com.br>
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
class Csv implements ConvertableArray
{
    /** @var string Delimitador para informa��es do CSV. */
    public static $delimiter = ';';

    /**
     * @inheritDoc
     */
    public static function toArray($data)
    {
        if (!is_string($data)) {
            throw new \Exception('Invalid input csv.');
        }

        $data = array_filter(explode("\n", $data));
        $header = explode(self::$delimiter, reset($data));
        unset($data[0]);

        $convertedArray = [];
        foreach ($data as $lineNumber => $line) {

            $line = explode(self::$delimiter, $line);

            foreach ($line as $key => $value) {

                $convertedArray[$lineNumber][trim($header[$key])] = $value;
            }
        }

        return $convertedArray;
    }

    /**
     * @inheritDoc
     */
    public static function fromArray($data)
    {
        if (!is_array($data) || count($data['content']) > 1) {
            throw new \Exception('Invalid input array.');
        }

        $header = implode(self::$delimiter, $data['header']);

        $convertedCsv = [$header];
        foreach ($data['content'] as $value) {

            $invalidArray = false;

            array_walk($value, function($v) use (&$invalidArray) {
                if (is_array($v)) {
                    $invalidArray = true;
                }
            });

            if ($invalidArray) {
                throw new \Exception('Invalid input array.');
            }

            $convertedCsv[] = implode(self::$delimiter, $value);
        }

        return implode("\n", $convertedCsv);
    }
}
