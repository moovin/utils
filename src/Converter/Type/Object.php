<?php

namespace Moovin\Utils\Converter\Type;

use Moovin\Utils\Converter\Contract\ConvertableArray;
use ReflectionClass;

/**
 * Trata das convers�es de objetos para array ou vice versa
 *
 * @author Matheus Gonzaga <matheus.gonzaga@moovin.com.br>
 * @author Gabriel Anhaia <gabriel.silva@moovin.com.br>
 */
class Object implements ConvertableArray
{
    /**
     * {@inheritdoc}
     */
    public static function fromArray($data)
    {
        if (empty($data)) {
            return null;
        }

        $object = new \stdClass();
        foreach ($data as $key => $value) {
            $object->{$key} = is_array($value) ? self::fromArray($value) : $value;
        }
        return $object;
    }

    /**
     * {inheritdoc}
     */
    public static function toArray($data)
    {
        if (empty($data)) {
            return null;
        }

        if (is_array($data) || $data instanceof \stdClass) {
            $attributes = (array) $data;

            foreach ($attributes as $name => $attribute) {
                if (is_object($data)) {
                    $name = self::formatAttributeName($name, $data);
                }

                $converted[$name] = self::toArray($attribute);
            }

            return $converted;
        } elseif (is_object($data)) {

            return self::convertReflectionClass($data);
        }
        return $data;
    }

    /**
     * Remove namespace e espa�os do nome do atributo.
     *
     * @param string $name Nome do atributo.
     * @param mixed $data Objeto no qual o atributo faz parte.
     *
     * @return string Nome do atributo formatado.
     */
    private static function formatAttributeName($name, $data)
    {
        if (empty($data) || empty($name)) {
            return '';
        }

        if (!empty(get_class($data))) {
            $name = str_replace(get_class($data), '', $name);
        }

        return trim($name);
    }

    /**
     * @param $data
     *
     * @return array
     */
    private static function convertReflectionClass($data)
    {
        $reflectionClass = new ReflectionClass($data);
        $array = [];
        foreach ($reflectionClass->getProperties() as $property) {
            $property->setAccessible(true);
            $array[$property->getName()] = $property->getValue($data);
        }
        return $array;
    }
}
