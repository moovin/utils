<?php

namespace Moovin\Utils\Converter\Type;

use Moovin\Utils\Converter\Contract\ConvertableArray;
use Moovin\Utils\Converter\Exception\InvalidTypeException;
/**
 * Trata das convers�es de array para array.
 * Este conversor � equivalente a uma convers�o sem a��o
 *
 * @author Matheus Gonzaga <matheus.gonzaga@moovin.com.br>
 */
class NoConvertable implements ConvertableArray
{

    /**
     * @inheritdoc
     */
    public static function fromArray($data)
    {
        if (!is_array($data)) {
            throw new InvalidTypeException('Dado informado n�o � um Array v�lido');
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public static function toArray($data)
    {
        if (is_array($data)) {
            return $data;
        }

        return [$data];
    }

}
