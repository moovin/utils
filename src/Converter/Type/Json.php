<?php
namespace Moovin\Utils\Converter\Type;

use Moovin\Utils\Converter\Contract\ConvertableArray;
use Moovin\Utils\Converter\Exception\JsonException;
use Moovin\Utils\Encoder\Type\Utf8;

/**
 * Trata das convers�es de JSON para array ou vice versa
 *
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
class Json implements ConvertableArray
{
    /**
     * Converte pra array os dados recebidos pelo par�metro do tipo do conversor
     *
     * @param string $data Json a ser convertido para array.
     *
     * @return array JSON decode do array.
     *
     * @throws \JsonException Caso ocorra algum erro na hora de fazer o decode.
     */
    public static function toArray($data)
    {
        $mustReturnArray = true;

        $array = json_decode($data, $mustReturnArray);

        self::jsonLastError($data);

        if (!is_array($array)) {
            $array = json_decode($array, $mustReturnArray);

            self::jsonLastError($data);
        }

        return $array;
    }

    /**
     * Converte para JSON o array recebido nopar�metro.
     *
     * @param array $data Array a ser convertido para json.
     * @param int $options Op��es do m�todo json_encode.
     *
     * @return string JSON encode do array.
     *
     * @throws JsonException Caso ocorra algum erro na hora de fazer o encode.
     */
    public static function fromArray($data, $options = 0)
    {
        $json_file = json_encode($data);

        try {
            self::jsonLastError($data);
        } catch(JsonException $exception) {
            if (json_last_error() == JSON_ERROR_UTF8) {
                $json_file = json_encode(Utf8::encode($data), $options);
            } else {
                throw $exception;
            }
        }

        return $json_file;
    }

    /**
     * Trata dos erros causados na hora de fazer o json_decode ou json_encode.
     * Lan�a uma exce��o JsonException sendo na mensagem da exce��o a mensagem
     * sugerida do erro no link.
     *
     * @see http://php.net/manual/pt_BR/function.json-last-error.php#refsect1-function.json-last-error-returnvalues
     *
     * @throws \JsonException Se o n�mero do json_last_error for algum erro.
     */
    private static function jsonLastError($data)
    {
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return;
                break;
            case JSON_ERROR_DEPTH:
                $jsonException = new JsonException('A profundidade m�xima da pilha foi excedida');
                $jsonException->setData($data);
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $jsonException =  new JsonException('JSON inv�lido ou mal formado');
                $jsonException->setData($data);
                break;
            case JSON_ERROR_CTRL_CHAR:
                $jsonException = new JsonException('Erro de caractere de controle, possivelmente codificado incorretamente');
                $jsonException->setData($data);
                break;
            case JSON_ERROR_SYNTAX:
                $jsonException = new JsonException('Erro de sintaxe');
                $jsonException->setData($data);
                break;
            case JSON_ERROR_UTF8:
                $jsonException = new JsonException('caracteres UTF-8 malformado , possivelmente codificado incorretamente');
                $jsonException->setData($data);
                break;
            default:
                $jsonException = new JsonException('Erro desconhecido');
                $jsonException->setData($data);
                break;
        }

        throw $jsonException;
    }
}