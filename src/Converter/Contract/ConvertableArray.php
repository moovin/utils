<?php
namespace Moovin\Utils\Converter\Contract;

/**
 * Interface com m�todos que o conversor deve implementar para ser tratado
 * como convert�vel do seu tipo para array e de array para o seu tipo.
 *
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
interface ConvertableArray
{
    /**
     * Converte pra array os dados recebidos pelo par�metro do tipo do conversor
     *
     * @param mixed $data Dados que ser� convertido para array.
     *
     * @return array Dados convertidos para array.
     */
    public static function toArray($data);

    /**
     * Converte para o tipo do conversor os dados recebidos pelo pr�metro 
     * do tipo array.
     *
     * @param array $data Array que ser� convertido para o tipo do conversor.
     *
     * @return mixed Dados convertidos para o tipo do conversor.
     */
    public static function fromArray($data);
}
