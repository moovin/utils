<?php
namespace Moovin\Utils\Converter;

/**
* Contém funções para fazer conversão de unidades.
* Converte unidades de dimensão e peso.
*
* @author Dantiéris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
*/
class Unity
{
    /** Constantes que contém as dimenções disponíveis */

    /** @var string Unidade de dimensão métro. */
    const DIMENSION_METER = 'm';

    /** @var string Unidade de dimensão centímetro. */
    const DIMENSION_CENTIMETER = 'cm';

    /** @var string Unidade de dimensão milímetro. */
    const DIMENSION_MILLIMETER = 'mm';

    /** @var string Unidade de peso kilograma. */
    const UNIT_WEIGHT_KILOGRAM = 'kg';

    /** @var string Unidade de peso grama. */
    const UNIT_WEIGTH_GRAM = 'g';

    /**
     * Converte um valor de centímetro para outra dimenção (milímetro ou metro).
     *
     * @param float $measure Valor da medida em centímetros a ser convertida.
     * @param string $dimensionTo Dimensão que o valor será convertido.
     *
     * @return float Resultado da conversão.
     */
    public static function dimensionFromCentimeter($measure, $dimensionTo = self::DIMENSION_CENTIMETER)
    {
        switch ($dimensionTo) {
            case self::DIMENSION_MILLIMETER:
                return $measure * 10;
                break;
            case self::DIMENSION_METER:
                return $measure / 100;
                break;
            case self::DIMENSION_CENTIMETER:
            default:
                return $measure;
        }
    }

    /**
     * Converte um valor de gramas para kilogramas.
     *
     * @param float $weight Valor do peso em gramas a ser convertido.
     * @param string $unitWeightTo Unidade de peso qeu o valor será conevrtido.
     *
     * @return float Resultado da conversão.
     */
    public static function weightFromGram($weight, $unitWeightTo = self::UNIT_WEIGTH_GRAM)
    {
        switch ($unitWeightTo) {
            case self::UNIT_WEIGHT_KILOGRAM:
                return $weight / 1000;
                break;
            case self::UNIT_WEIGTH_GRAM:
            default:
                return $weight;
        }
    }
}