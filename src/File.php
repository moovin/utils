<?php
namespace Moovin\Utils;

/**
 * Lib para manipulação de arquivos e diretórios
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class File
{
    /**
     * Obtém os arquivos de um diretório
     *
     * @param string $dirPath
     *
     * @param boolean $absolutePath
     *
     * @return array
     */
    public static function readDirectory($dirPath, $absolutePath = false)
    {
        $files = array_diff(scandir($dirPath), [".", ".."]);

        if ($absolutePath) {
            foreach ($files as &$file) {
                $file = realpath($dirPath . DIRECTORY_SEPARATOR . $file);
            }
        }

        return $files;
    }

    /**
     * Obtém os arquivos de um diretório recursivamente
     *
     * @param string $dirPath
     *
     * @param boolean $absolutePath
     *
     * @return array
     */
    public static function readDirectoryRecursive($dirPath, $absolutePath = false)
    {
        $files = [];

        foreach (array_diff(scandir($dirPath), [".", ".."]) as $file) {
            $filePath = $dirPath . DIRECTORY_SEPARATOR . $file;

            if (is_dir($filePath)) {
                foreach (self::readDirectoryRecursive($filePath, $absolutePath) as $innerFile) {
                    $files[] = $absolutePath ? $innerFile : $file . DIRECTORY_SEPARATOR . $innerFile;
                }
            } else {
                $files[] = $absolutePath ? realpath($filePath) : $file;
            }
        }

        return $files;
    }

    /**
     * Remove recursivamente um diretório com todo o seu conteúdo
     *
     * Semelhante ao comando <samp>rm -rf foo<samp>
     *
     * @param string $dirPath
     *
     * @return boolean
     */
    public static function removeDirectoryRecursive($dirPath)
    {
        foreach (array_diff(scandir($dirPath), [".", ".."]) as $file) {
            $filePath = $dirPath . DIRECTORY_SEPARATOR . $file;

            if (is_dir($filePath)) {
                self::removeDirectoryRecursive($filePath);
            } else {
                unlink($filePath);
            }
        }

        return rmdir($dirPath);
    }

    /**
     * Obtém os arquivos de um diretório com base na expressão regular
     *
     * @param string $path
     *
     * @param string $pattern
     *
     * @param string $delimiter
     *
     * @param string $modifier
     *
     * @return array
     */
    public static function findRegexpDirectory($path, $pattern, $delimiter = "~", $modifier = "i")
    {
        $regexp = $delimiter . $pattern . $delimiter . $modifier;

        $files = [];

        foreach (self::readDirectory($path, true) as $file) {
            if (preg_match($regexp, $file)) {
                $files[] = $file;
            }
        }

        return $files;
    }

    /**
     * Obtém os arquivos de um diretório de forma recursiva com base na expressão regular
     *
     * @param string $path
     *
     * @param string $pattern
     *
     * @param string $delimiter
     *
     * @param string $modifier
     *
     * @return array
     */
    public static function findRegexpDirectoryRecursive($path, $pattern, $delimiter = "~", $modifier = "i")
    {
        $regexp = $delimiter . $pattern . $delimiter . $modifier;

        $files = [];

        foreach (self::readDirectoryRecursive($path, true) as $file) {
            if (preg_match($regexp, $file)) {
                $files[] = $file;
            }
        }

        return $files;
    }

    /**
     * Cria um arquivo temporário e retorna o caminho
     *
     * @param string|null $prefix Prefixo do nome do arquivo
     *
     * @return string|false
     */
    public static function createTemporaryFile($prefix = null)
    {
        if (!$prefix) {
            $prefix = str_replace(["/", "+", "="], "0", base64_encode(dechex(mt_rand(0, 0xFFFFFF))));
        }

        return tempnam(sys_get_temp_dir(), $prefix);
    }

    /**
     * Gera uma caminho na pasta temporária e a retorna
     *
     * @param string|null $prefix Prefixo do nome do caminho
     *
     * @return string|false
     */
    public static function generateTemporaryPath($prefix = null)
    {
        if (!$path = self::createTemporaryFile($prefix)) {
            return false;
        }

        if (!unlink($path)) {
            return false;
        }

        return $path;
    }

    /**
     * Cria um diretório temporário e retorna o caminho
     *
     * @param string|null $prefix Prefixo do nome do diretório
     *
     * @return string|false
     */
    public static function createTemporaryDirectory($prefix = null)
    {
        if (!$path = self::generateTemporaryPath($prefix)) {
            return false;
        }

        if (!mkdir($path)) {
            return false;
        }

        return $path;
    }

    /**
     * Obtém a extensão do arquivo passado
     *
     * @param string $file Nome ou caminho do arquivo
     *
     * @return string
     */
    public static function getExtension($file)
    {
        if (!preg_match("~\.([^./\\\\]+)$~", $file, $extension)) {
            return "";
        }

        return $extension[1];
    }

    /**
     * Obtém o MIME do arquivo
     *
     * @param string $file Nome ou caminho do arquivo
     *
     * @param bool $useVanillaOnFail Utiliza a função padrão do PHP para
     * detecção de MIME quando não encontra MIME a extensão no arquivo passado
     *
     * @return string
     */
    public static function getMime($file, $useVanillaOnFail = true)
    {
        static $mimeList = false;

        if (!$mimeList) {
            $mimeList = include(__DIR__ . "/../resources/file/mime-list.php");
        }

        $extension = self::getExtension($file);

        if (isset($mimeList[$extension])) {
            return $mimeList[$extension];
        }

        if ($useVanillaOnFail) {
            if (!file_exists($file)) {
                $file = realpath($file);
            }

            if (file_exists($file)) {
                $mime = mime_content_type($file);

                if ($mime) {
                    return $mime;
                }
            }
        }

        return "application/octet-stream";
    }
}
