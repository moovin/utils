<?php
namespace Moovin\Utils;

/**
 * Lib "Array"
 * @author Kelvin Gluszszak Lopes <kelvin.lopes@moovin.com.br>
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class Vector
{
    /**
     * Formata array multidimensional para padrao dot notation
     * @param array $toConvert Array a converter
     * @return array
     */
    public static function toDotNotation($toConvert)
    {
        $ritit = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($toConvert));
        $result = [];
        foreach ($ritit as $leafValue) {
            $keys = [];
            foreach (range(0, $ritit->getDepth()) as $depth) {
                $keys[] = $ritit->getSubIterator($depth)->key();
            }
            $result[join('.', $keys)] = $leafValue;
        }
        return $result;
    }

    /**
     * Formata padrao dot notation para array multidimensional
     * @param mixed $path Path para converter
     * @param mixed $value Valor
     * @return array
     */
    public static function fromDotNotation($path, $value = null)
    {
        $toConvert = $path;
        if (!is_array($toConvert)) {
            $toConvert = [
                $path => $value
            ];
        }
        $return = [];
        foreach ($toConvert as $path => $value) {
            $item =& $return;
            foreach (explode('.', $path) as $step) {
                $item =& $item[$step];
            }
            $item = $value;
        }
        return $return;
    }

    /**
     * Verifica se um array esta dentro de outro
     * @param array $neddle
     * @param array $haystack
     * @return boolean
     */
    public static function isContainedArray(array $neddle, array $haystack)
    {
        return (boolean) count(array_intersect($haystack, $needle)) == count($needle);
    }

    /**
     * Retorna valor mais proximo encontrado no array
     * @param integer $search Valor a encontrar
     * @param array $arr Array a verificar
     * @return integer
     */
    public static function getClosestInteger($search, $arr) {
        $closest = null;
        foreach($arr as $item) {
            if($closest == null || abs($search - $closest) > abs($item - $search)) {
                $closest = $item;
            }
        }
        return $closest;
    }

    /**
     * Computa a interseção de array comparando pelas chaves recursivamente
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public static function recursiveIntersectKey(array $array1, array $array2) {
        $array1 = array_intersect_key($array1, $array2);
        foreach ($array1 as $key => &$value) {
            if (is_array($value)) {
                $value = self::recursiveIntersectKey($value, $array2[$key]);
            }
        }
        $array1 = array_filter($array1);
        return $array1;
    }

    /**
     * Verifica se existe ou não um índice em uma array ou objeto.
     * @param array|object $arrobj Array ou objeto em questao
     * @param mixed $index Indice da Array/Objeto
     * @return boolean Se existe ou nao o indice
     */
    public static function exists(&$arrobj, $index)
    {
        if (!is_string($index) && !is_int($index)) {
            if (is_numeric($index) || is_bool($index)) {
                $index = (integer) $index;
            } else {
                $index = "";
            }
        }

        if (is_array($arrobj)) {
            return isset($arrobj[$index]) || array_key_exists($index, $arrobj);
        } elseif (is_object($arrobj)) {
            return isset($arrobj->$index) || array_key_exists($index, $arrobj);
        } else {
            return false;
        }
    }

    /**
     * Retorna um valor de um array ou object multidimensional validando se o mesmo existe
     * @param array|object &$arrobj Variável a ser procurada
     * @param array|string $key Índice(s) da array ou objeto
     * @param mixed &$exists Ponteiro para o valor da existência da índice
     * @return mixed Valor da array ou objeto no índice especificado ou NULL para inexistente
     */
    public static function value(&$arrobj, $index, &$exists = null)
    {
        if (!is_array($index)) {
            $index = \explode("|", $index);
        }

        $key = 0;

        $value = &$arrobj;

        $exists = false;

        if (self::exists($index, $key)) {
            do {
                if (self::exists($value, $index[$key])) {
                    if (is_array($value)) {
                        $value = &$value[$index[$key++]];
                    } else {
                        $value = &$value->{$index[$key++]};
                    }
                } else {
                    return null;
                }
            } while (self::exists($index, $key));
        } else {
            return null;
        }

        $exists = true;

        return $value;
    }

    /**
     * Extrai da array os parametros passados, retornando fora dela.
     * @param array $array Variável aonde vai ser extraído os dados
     * @param array $parameters Variável aonde define quais valores serão removidos
     * @param mixed $default Nome da variável enviada no primeiro parâmetro
     * @return array
     */
    public static function extractFrom(array $array, array $parameters, $default = null)
    {
        if (!$default) {
            reset($array);

            $default = key($array);

            $array = $array[$default];
        }

        $parameters = array_flip($parameters);

        $extract = array_intersect_key($array, $parameters);

        $extract[$default] = array_diff_key($array, $extract);

        return $extract;
    }

    /**
     * Une Arrays
     * @param array $arrays,...
     * @return array
     */
    public static function union(... $arrays)
    {
        if (!$arrays) {
            return [];
        }

        $union = array_pop($arrays);

        if (!$arrays) {
            return $union;
        } else {
            foreach ($arrays as $array) {
                foreach ($array as $key => $value) {
                    if (is_int($key)) {
                        $union[] = $value;
                    } elseif (is_array($value) && (isset($union[$key]) && is_array($union[$key]))) {
                        $union[$key] = self::union($union[$key], $value);
                    } else {
                        $union[$key] = $value;
                    }
                }
            }
        }

        return $union;
    }

    /**
     * Retorna se array é associativo
     * @param array $array
     * @return bool
     */
    public static function isAssociative(array $array)
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }

    /**
     * Remove os índices do array se eles existirem
     * @param array $array
     * @param array $keys
     * @return array
     */
    public static function removeKeys(array $array, array $keys)
    {
        return array_diff_key($array, array_flip($keys));
    }

    /**
     * Ordena e remove os índices de uma array conforme a ordem
     * @param array $array Array a ser ordenada
     * @param array $order Ordem dos índices, os não presentes serão removidos
     * @result array
     */
    public static function reorder(array $array, array $order)
    {
        $order = array_flip($order);

        foreach ($order as $index => &$value) {
            if (self::exists($array, $index)) {
                $value = $array[$index];
            } else {
                unset($order[$index]);
            }
        }

        return $order;
    }

    /**
     * Traduz os índices de uma array
     *<code>
     *  print_r(Vector::translateIndex([
     *      "unsetted",
     *      "foo",
     *      "bar"
     *  ], [
     *      "foo" => 1,
     *      "bar" => 2
     *  ]);
     *  // Output:
     *  // Array
     *  // (
     *  //     [foo] => foo
     *  //     [bar] => bar
     *  // )
     *</code>
     * @param array $array
     * @param array $indexes
     * @return array
     */
    public static function translateIndex(array $array, array $indexes)
    {
        foreach ($indexes as $index => &$value) {
            if (self::exists($array, $value)) {
                $value = $array[$value];
            } else {
                unset($indexes[$index]);
            }
        }

        return $indexes;
    }

    /**
     * Verifica se todos os campos passados estão presentes na array
     * @param array $array
     * @param array $fields
     * @return boolean
     */
    public static function hasAllFields(array $array, array $fields)
    {
        return count(array_intersect_key($array, array_flip($fields))) === count($fields);
    }

    /**
     * Verifica o array tem apenas os campos especificados
     * @param array $array
     * @param array $fields
     * @return boolean
     */
    public static function hasOnlyFields(array $array, array $fields)
    {
        $fields = array_flip($fields);

        return !array_diff_key($array, $fields) && !array_diff_key($fields, $array);
    }

    /**
     * Ordena a array conforme o lista passada
     *<code>
     *  print_r(Vector::sortWithList([
     *      4 => "yolo",
     *      5 => "foo",
     *      3 => "bar",
     *      7 => "sup"
     *  ], [
     *      7,
     *      5,
     *      3,
     *      4
     *  ]);
     *  // Output:
     *  // Array
     *  // (
     *  //     [7] => sup
     *  //     [5] => foo
     *  //     [3] => bar
     *  //     [4] => yolo
     *  // )
     *</code>
     * @param array $array Array a ser ordenada
     * @param array $list Lista com os valores dos índices da primeira array
     * @return array
     */
    public static function sortWithList(array $array, array $list)
    {
        ksort($list);

        $orderedList = array_combine($list, $list);

        return Vector::translateIndex($array, $orderedList);
    }

    /**
     * Verifica se um array tem multiníveis
     * @param array $array
     * @return boolean
     */
    public static function isMultidimensional(array $array)
    {
        foreach ($array as $node) {
            if (is_array($node)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Converte um array para objeto
     * @param array $array
     * @return object
     */
    public static function toObject(array $array)
    {
        foreach ($array as &$node) {
            if (is_array($node)) {
                $node = self::toObject($node);
            }
        }

        return (object) $array;
    }

    /**
     * Converte arrays associativos em objeto e um mantém arrays indexadas
     * @param array $array
     * @return object|array
     */
    public static function toObjectSmarty(array $array)
    {
        return json_decode(json_encode($array));
    }

    /**
     * Executa o callback em todas os arrays juntando o resultado em um único.
     * O callback irá receber dois paramêtros: o valor e a chave do array. Os
     * resultados podem ser: um array com dois índices, o primeiro é o valor e
     * e o segundo a chave para o array de retorno; Ou apenas um valor escalar
     * que será adicionado ao "final" do array
     * retorno
     *<code>
     *  print_r(Vector::map(function ($value, $key) {
     *      return [
     *          $key,
     *          $value
     *      ]
     *  }, [
     *      "yolo" => "sup",
     *      "foo" => "bar",
     *      "test" => "tset"
     *  ]);
     *  // Output:
     *  // Array
     *  // (
     *  //     [sup] => yolo
     *  //     [bar] => foo
     *  //     [tset] => test
     *  // )
     *</code>
     * @param callable $callback
     * @param array,... $arrays
     * @return array
     */
    public static function map(callable $callback, array ...$arrays)
    {
        $mapped = [];

        $whenEmptyKey = [
            1 => null
        ];

        foreach ($arrays as $array) {
            foreach ($array as $key => $value) {
                $_ = (array) call_user_func($callback, $value, $key);

                list($_value, $_key) = $_ + $whenEmptyKey;

                if (is_null($_key)) {
                    $mapped[] = $_value;
                } else {
                    $mapped[$_key] = $_value;
                }
            }
        }

        return $mapped;
    }
}
