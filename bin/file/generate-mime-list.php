<?php

/**
 * Atualiza a lista de MIMEs utilizados pelo método 
 * \Moovin\Utils\File::getMime()
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 *
 * @see http://php.net/manual/pt_BR/function.mime-content-type.php#107798
 */

$filePath = __DIR__ . "/../../resources/file/mime-list.php";

$mimeTypesUrl = "http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types";

$mimeTypePattern = "~^(?:#\s*+)?(?<mime>\S+)(\s{2,})(?<extensions>(\s*\w+)*)$~m";

try {
    $mimeTypes = file_get_contents($mimeTypesUrl);

    if (!$mimeTypes) {
        throw new Exception("Impossível obter lista de tipos de MIMEs");
    }

    if (
        !preg_match_all($mimeTypePattern, $mimeTypes, $mimes, PREG_SET_ORDER)
    ) {
        throw new Exception("Impossível obter MIMES da lista");
    }

    $handle = fopen($filePath, "wb");

    if (!$handle) {
        throw new Exception("Não foi possível abrir o arquivo $filePath");
    }

    if (!fwrite($handle, "<?php\n\nreturn [\n")) {
        throw new Exception("Não foi possível escrever no arquivo $filePath");
    }

    foreach ($mimes as $mime) {
        $extensions = explode(" ", $mime["extensions"]);

        foreach ($extensions as $extension) {
            $line = sprintf(
                "    \"%s\" => \"%s\",\n",
                $extension,
                $mime["mime"]
            );

            fwrite($handle, $line);
        }
    }
    
    fwrite($handle, "];\n");

    fclose($handle);
} catch (Exception $exception) {
    echo $exception->getMessage();
    
    exit($exception->getLine() ?: 1);
}

echo "Arquivo atualizado com sucesso";
