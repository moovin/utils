<?php
namespace Moovin\Utils\Tests\Fixtures\Converter\Type;

/**
 * @todo Document class ObjectTestMock.
 *
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
class ObjectTestMock
{
    protected $attribute1;
    private $attribute2;
    public $attribute3;

    /**
     * Retorna a propriedade {@see ObjectTestMock::$attribute1}.
     *
     * @return mixed
     */
    public function getAttribute1()
    {
        return $this->attribute1;
    }

    /**
     * Define a propriedade {@see ObjectTestMock::$attribute1}.
     *
     * @param mixed $attribute1
     *
     * @return ObjectTestMock
     */
    public function setAttribute1($attribute1)
    {
        $this->attribute1 = $attribute1;
        return $this;
    }

    /**
     * Retorna a propriedade {@see ObjectTestMock::$attribute2}.
     *
     * @return mixed
     */
    public function getAttribute2()
    {
        return $this->attribute2;
    }

    /**
     * Define a propriedade {@see ObjectTestMock::$attribute2}.
     *
     * @param mixed $attribute2
     *
     * @return ObjectTestMock
     */
    public function setAttribute2($attribute2)
    {
        $this->attribute2 = $attribute2;
        return $this;
    }

    /**
     * Retorna a propriedade {@see ObjectTestMock::$attribute3}.
     *
     * @return mixed
     */
    public function getAttribute3()
    {
        return $this->attribute3;
    }

    /**
     * Define a propriedade {@see ObjectTestMock::$attribute3}.
     *
     * @param mixed $attribute3
     *
     * @return ObjectTestMock
     */
    public function setAttribute3($attribute3)
    {
        $this->attribute3 = $attribute3;
        return $this;
    }
}
