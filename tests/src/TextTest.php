<?php
namespace Moovin\Utils\Tests;
use Moovin\Utils\Text;

/**
 * Teste para a util de Texto
 *
 * @author Gabriel Anhaia <gabriel.silva@moovin.com.br>
 */
class TextTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testa a convers�o de texto para SnakeCase
     * @dataProvider textDataProvider
     */
    public function testToSnakeCase($originalText, $camelCaseText, $snakeCaseText)
    {
        $this->assertEquals($snakeCaseText, Text::toSnakeCase($originalText));
    }

    /**
     * Testa a convers�o de texto para CamelCase
     * @dataProvider textDataProvider
     */
    public function testToCamelCase($originalText, $camelCaseText, $snakeCaseText)
    {
        $this->assertEquals($camelCaseText, Text::toCamelCase($originalText));
    }

    /**
     * Testa a convers�o de lowerCamelCase para snake_case
     */
    public function testCamelToSnake()
    {
        $this->assertEquals('moovin_test_case', Text::camelToSnake('moovinTestCase'));
    }

    /**
     * Provedor de dados para testes de convers�es de texto.
     * 
     * @return array
     */
    public function textDataProvider()
    {
        return [
            [
                'originalText' => 'Application developer',
                'camelCaseText' => 'applicationDeveloper',
                'snakeCaseText' => 'Application_developer',
            ],
            [
                'originalText' => 'developer',
                'camelCaseText' => 'developer',
                'snakeCaseText' => 'developer',
            ],
            [
                'originalText' => 'analysis and SystemS deVeLopment',
                'camelCaseText' => 'analysisAndSystemsDevelopment',
                'snakeCaseText' => 'analysis_and_SystemS_deVeLopment',
            ],
            [
                'originalText' => 'Object-oriented programming',
                'camelCaseText' => 'objectOrientedProgramming',
                'snakeCaseText' => 'Object_oriented_programming',
            ],
            [
                'originalText' => '10 test with numbers',
                'camelCase' => '10TestWithNumbers',
                'snakeCase' => '10_test_with_numbers',
            ],
            [
                'originalText' => 'test with � accents �',
                'camelCase' => 'testWithEAccentsA',
                'snakeCase' => 'test_with_E_accents_a',
            ]
        ];
    }
}