<?php

namespace Moovin\Utils\Tests;

use PHPUnit\Framework\TestCase as PHPUnitTestCase;
use Moovin\Utils\Vector;

/**
 * Classe base para teste
 *
 * @package Moovin\Utils\Tests
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
abstract class TestCase extends PHPUnitTestCase
{
    /**
     * Retorna o caminho de arquivo de fixtures
     *
     * @param string $file
     *
     * @return string
     */
    public function getFixtureFilePath($file)
    {
        $file = ltrim($file, "/\\");

        return __DIR__ . "/../fixtures/{$file}";
    }

    /**
     * Retorna conteúdo de arquivo de fixtures
     *
     * @param string $file
     *
     * @param bool $useCache
     *
     * @return string
     */
    public function getFixtureFileContent($file, $useCache = true)
    {
        static $cache = [];

        $filePath = $this->getFixtureFilePath($file);

        if (!$useCache) {
            return file_get_contents($filePath);
        }

        if (!Vector::exists($cache, $file)) {
            $cache[$file] = file_get_contents($filePath);
        }

        return $cache[$file];
    }
}
