<?php

namespace Moovin\Utils\Tests;

use Moovin\Utils\State;

/**
 * Testes para as opera��es de data
 *
 * @author Gabriel Anhaia <gabriel.silva@moovin.com.br>
 */
class StateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Converte um nome de estado para as suas iniciais.
     *
     * @param string $fullName Nome completo do estado.
     * @param string $initialName Iniciais do nome do estado.
     *
     * @dataProvider fullStateNameToInitialNameDataProvider
     */
   public function testFullStateNameToInitialName($fullName, $initialName)
   {
        $this->assertEquals(
            $initialName,
            State::getInitialsState($fullName)
        );
   }

    /**
     * Provedor de dados para testes de convers�o de nome de estados para siglas.
     *
     * @return array
     */
   public function fullStateNameToInitialNameDataProvider()
   {
       return [
           [
               'fullName' => 'State Not Found',
               'initialName' => ''
           ],
           [
               'fullName' => 'S�o Paulo',
               'initialName' => 'SP'
           ],
           [
               'fullName' => 'RIO Grande do Sul',
               'initialName' => 'RS'
           ],
           [
               'fullName' => 'SAO PAULO',
               'initialName' => 'SP'
           ],
           [
               'fullName' => 'paran�',
               'initialName' => 'PR'
           ],
           [
               'fullName' => 'Mato GrOsso',
               'initialName' => 'MT'
           ]
       ];
   }
}
