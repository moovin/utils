<?php

namespace Moovin\Utils\Tests\Converter\Type;

use Moovin\Utils\Converter\Type\NoConvertable;

/**
 * Teste para o conversor de tipo array (sem convers�o)
 *
 * @author Matheus Gonzaga <matheus.gonzaga@moovin.com.br>
 */
class NoConvertableTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Moovin\Utils\Converter\Type\NoConvertable::toArray
     */
    public function testToArray()
    {
        $this->assertEquals(
            ['attr1' => ['attr2' => 2], 'attr3' => 3],
            NoConvertable::toArray(['attr1' => ['attr2' => 2], 'attr3' => 3])
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\NoConvertable::fromArray
     */
    public function testFromArray()
    {
        $this->assertEquals(
            ['attr1' => ['attr2' => 2], 'attr3' => 3],
            NoConvertable::fromArray(['attr1' => ['attr2' => 2], 'attr3' => 3])
        );
    }

}