<?php

namespace Moovin\Utils\Tests\Converter\Type;

use Moovin\Utils\Converter\Type\Xml;

/**
 * Teste para o conversor de tipo xml
 *
 * @author Matheus Gonzaga <matheus.gonzaga@moovin.com.br>
 */
class XmlTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Moovin\Utils\Converter\Type\Xml::fromArray
     */
    public function testFromArray()
    {
        $this->assertEquals(
            Xml::build('<root><teste>teste</teste></root>'),
            Xml::fromArray(['root' => ['teste' => 'teste']])
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Xml::toArray
     */
    public function testToArrayWithBuilld()
    {
        $this->assertEquals(
            ['root' => ['teste' => 'teste']],
            Xml::toArray(Xml::build('<root><teste>teste</teste></root>'))
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Xml::toArray
     */
    public function testToArrayWithoutBuild()
    {
        $this->assertEquals(
            ['root' => ['teste' => 'teste']],
            Xml::toArray('<root><teste>teste</teste></root>')
        );
    }
}