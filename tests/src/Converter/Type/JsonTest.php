<?php

namespace Moovin\Utils\Tests\Converter\Type;

use Moovin\Utils\Converter\Type\Json;

/**
 * Teste para o conversor de tipo json
 *
 * @author Matheus Gonzaga <matheus.gonzaga@moovin.com.br>
 */
class JsonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Moovin\Utils\Converter\Type\Json::toArray
     */
    public function testToArray()
    {
        $this->assertEquals(
            ['teste' => 'teste'],
            Json::toArray('{"teste":"teste"}')
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Json::toArray
     */
    public function testToArrayQuoted()
    {
        $this->assertEquals(
            ['teste' => 'teste'],
            Json::toArray('"{ \"teste\":\"teste\" }"')
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Json::fromArray
     */
    public function testFromArray()
    {
        $this->assertEquals(
            '{"teste":"teste"}',
            Json::fromArray(['teste' => 'teste'])
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Json::fromArray
     */
    public function testFromArrayUtf8()
    {
        $this->assertEquals(
            '{"t\u00e9st\u00ea":"test\u00e8"}',
            Json::fromArray(['t�st�' => 'test�'])
        );
    }
}