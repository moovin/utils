<?php

namespace Moovin\Utils\Tests\Converter\Type;

use Moovin\Utils\Converter\Type\Csv;

/**
 * Teste para o conversor de tipo csv.
 *
 * @author Thiago Hofmeister <thiago.souza@moovin.com.br>
 * @author Danti�ris Castilhos Rabelini <dantieris.rabelini@moovin.com.br>
 */
class CsvTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers \Moovin\Utils\Converter\Type\Csv::toArray
     */
    public function testToArray()
    {
        $this->assertEquals(
            [1 => [
                'sku' => '1323_111_0',
                'product-sku' => '1323_111_0'
            ]],
            Csv::toArray("sku;product-sku\n1323_111_0;1323_111_0\n")
        );
    }

    /**
     * @covers \Moovin\Utils\Converter\Type\Csv::fromArray
     */
    public function testFromArray()
    {
        $this->assertEquals(
            "sku;product-sku\n1323_111_0;1323_111_0",
            Csv::fromArray([
                'header' => ['sku', 'product-sku'],
                'content' => [[
                    'sku' => '1323_111_0',
                    'product-sku' => '1323_111_0'
                ]]
            ])
        );
    }
}
