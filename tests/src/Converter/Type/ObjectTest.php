<?php

namespace Moovin\Utils\Tests\Converter\Type;

use Moovin\Utils\Converter\Type\Object;
use Moovin\Utils\Tests\Fixtures\Converter\Type\ObjectTestMock;

/**
 * Teste para o conversor de tipo object
 *
 * @author Matheus Gonzaga <matheus.gonzaga@moovin.com.br>
 * @author Gabriel anhaia <gabriel.silva@moovin.com.br>
 */
class ObjectTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Moovin\Utils\Converter\Type\Object::toArray
     */
    public function testToArray()
    {
        $objTeste = new \stdClass();
        $objTeste->attr1 = new \stdClass();
        $objTeste->attr1->attr2 = 2;
        $objTeste->attr3 = 3;

        $this->assertEquals(
            ['attr1' => ['attr2' => 2], 'attr3' => 3],
            Object::toArray($objTeste)
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Object::toArray
     */
    public function testFromArray()
    {
        $arrayTest = [
            'level1' => [
                'level21' => [
                    'level31' => 'value',
                    'level32' => 'value2',
                    'level33' => [
                        [
                            'level41' => 'value',
                            'level42' => 2
                        ],
                        [
                            'value',
                            'value2',
                        ]
                    ]
                ]
            ],
            'level12' => 'value'
        ];

        $objectTest = new \stdClass();
        $objectTest->level1 = new \stdClass();
        $objectTest->level12 = 'value';
        $objectTest->level1->level21 = new \stdClass();
        $objectTest->level1->level21->level31 = 'value';
        $objectTest->level1->level21->level32 = 'value2';
        $objectTest->level1->level21->level33 = new \stdClass();
        $objectTest->level1->level21->level33->{'0'} = new \stdClass();
        $objectTest->level1->level21->level33->{'0'}->level41 = 'value';
        $objectTest->level1->level21->level33->{'0'}->level42 = 2;
        $objectTest->level1->level21->level33->{'1'} = new \stdClass();
        $objectTest->level1->level21->level33->{'1'}->{'0'} = 'value';
        $objectTest->level1->level21->level33->{'1'}->{'1'} = 'value2';

        $this->assertEquals($objectTest, Object::fromArray($arrayTest));
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Object::toArray
     */
    public function testToArrayUsingArray()
    {
        $arrayTeste = [
            'attr1' => [
                'attr2' => 2,
            ],
            'attr3' => 3
        ];

        $this->assertEquals(
            ['attr1' => ['attr2' => 2], 'attr3' => 3],
            Object::toArray($arrayTeste)
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Object::toArray
     */
    public function testToArrayUsingArrayOfObjects()
    {
        $objTeste1 = new \stdClass();
        $objTeste1->attr1 = 1;

        $objTeste2 = new \stdClass();
        $objTeste2->attr2 = 2;

        $objTeste3 = new \stdClass();
        $objTeste3->attr3 = 3;

        $arrayTeste = [$objTeste1, $objTeste2, $objTeste3];

        $this->assertEquals(
            [['attr1' => 1], ['attr2' => 2], ['attr3' => 3]],
            Object::toArray($arrayTeste)
        );
    }

    /**
     * @covers Moovin\Utils\Converter\Type\Object::toArray
     */
    public function testToArrayUsingNamespace()
    {
        $obj = new ObjectTestMock();
        $obj->setAttribute1(1)->setAttribute2(2)->setAttribute3(3);

        $this->assertEquals(
            [
                'attribute1' => 1,
                'attribute2' => 2,
                'attribute3' => 3,
            ],
            Object::toArray($obj)
        );
    }
}