<?php

namespace Moovin\Utils\Tests;

use Moovin\Utils\Vector;

/**
 * Teste para Moovin\Utils\Vector
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class VectorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testa o método Vector::hasAllFields()
     *
     * @covers Moovin\Utils\Vector::hasAllFields
     */
    public function testHasAllFields()
    {
        $data = [
            "yolo" => true,
            "sup" => 2,
            "modafoca" => [],
            "doh" => "~~"
        ];

        $fields = [
            "yolo",
            "sup",
            "modafoca"
        ];

        $this->assertTrue(Vector::hasAllFields($data, $fields));

        unset($data["yolo"]);

        $this->assertFalse(Vector::hasAllFields($data, $fields));
    }

    /**
     * Testa o método Vector::hasOnlyFields()
     *
     * @covers Moovin\Utils\Vector::hasOnlyFields
     */
    public function testHasOnlyFields()
    {
        $data = [
            "yolo" => true,
            "sup" => 2,
            "modafoca" => []
        ];

        $fields = [
            "yolo",
            "sup",
            "modafoca"
        ];

        $this->assertTrue(Vector::hasOnlyFields($data, $fields));

        $data["doh"] = "~~";

        $this->assertFalse(Vector::hasOnlyFields($data, $fields));

        unset($data["doh"], $data["yolo"]);

        $this->assertFalse(Vector::hasOnlyFields($data, $fields));
    }

    /**
     * Testa o método Vector::sortWithList()
     *
     * @covers Moovin\Utils\Vector::sortWithList
     */
    public function testSortWithList()
    {
        $output =  Vector::sortWithList([
            4 => "yolo",
            5 => "foo",
            3 => "bar",
            7 => "sup"
        ], [
            7,
            5,
            3,
            4
        ]);

        $expected = [
            7 => "sup",
            5 => "foo",
            3 => "bar",
            4 => "yolo"
        ];

        $this->assertSame($expected, $output);
    }

    /**
     * Testa o método Vector::map()
     *
     * @dataProvider mapDataProvider
     *
     * @covers Moovin\Utils\Vector::map
     */
    public function testMap($callback, $data, $expected)
    {
        $this->assertSame($expected, Vector::map($callback, ...$data));
    }

    /**
     * Provedor de dados para testes de mapas de arrays
     *
     * @return array
     */
    public function mapDataProvider()
    {
        return [
            [
                function ($value) {
                    return array_filter(explode("|", $value, 2), "strlen") + [null, null];
                },
                [
                    [
                        "value1|key1",
                        "value2|key2",
                        "value3|key3",
                        "value4|key4",
                        "value0"
                    ],
                    [
                        "|key5"
                    ]
                ],
                [
                    "key1" => "value1",
                    "key2" => "value2",
                    "key3" => "value3",
                    "key4" => "value4",
                    0 => "value0",
                    "key5" => null
                ]
            ],
            [
                function ($value, $key) {
                    return array_map("trim", [$value, $key]);
                },
                [
                    [
                        "  YOLO " => "     yolo     ",
                        "  yolo " => "

                        YOLO

                        "
                    ]
                ],
                [
                    "YOLO" => "yolo",
                    "yolo" => "YOLO"
                ]
            ],
            [
                function ($value) {
                    return ucwords(strtolower($value));
                },
                [
                    [
                        "yolo sup modafoca",
                        "YOLO",
                        "SUP",
                        "sup modafoca",
                        "MODAFOCA"
                    ]
                ],
                [
                    "Yolo Sup Modafoca",
                    "Yolo",
                    "Sup",
                    "Sup Modafoca",
                    "Modafoca"
                ]
            ]
        ];
    }
}
