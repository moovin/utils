<?php

namespace Moovin\Utils\Tests;

use Moovin\Utils\Date;
use DateTime;
use DateInterval;

/**
 * Testes para as opera��es de data
 *
 * @author Matheus Gonzaga <matheus.gonzaga@moovin.com.br>
 */
class DateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Moovin\Utils\Date::getEarliestDate
     */
    public function testGetEarliestDate()
    {
        $dateA = new \DateTime('2016-03-01');
        $dateB = new \DateTime('2016-03-10');

        $this->assertEquals($dateA, Date::getEarliestDate($dateA, $dateB));
    }

    public function testAddBusinessDays()
    {
        $expected = new \DateTime('2016-04-19');
        $date = new Date(new DateTime('2016-04-13'));

        $date->addHoliday('14/04');
        $date->addHoliday('15/04');

        $added = $date->addBusinessDays(2);

        $this->assertEquals($expected, $added);
    }

    public function testCompareToBiggerThan()
    {
        $bigger = (new DateTime())->add(new DateInterval('P1D'));
        $date = new Date($bigger);

        $this->assertSame((int) 1, $date->compareTo(new DateTime()));
    }

    public function testCompareToEqualsTo()
    {
        $equals = (new DateTime())->add(new DateInterval('P1D'));
        $date = new Date($equals);

        $this->assertSame((int) 0, $date->compareTo($equals));
    }

    public function testCompareToSmallerThan()
    {
        $smaller = (new DateTime())->sub(new DateInterval('P1D'));
        $date = new Date($smaller);

        $this->assertSame((int) -1, $date->compareTo(new DateTime()));
    }
}
