<?php

namespace Moovin\Utils\Tests\Encode;

use Moovin\Utils\Tests\TestCase as BaseTestCase;
use Moovin\Utils\Encode\Detector;
use Moovin\Utils\Encode\Type;

/**
 * Classe de teste para a detecção de codificação
 *
 * @package Moovin\Utils\Tests\Encode
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class DetectorTest extends BaseTestCase
{
    /**
     * Teste para obter o conteúdo do arquivo com codificação UTF-8 válida
     *
     * @return string
     */
    public function testGetValidUTF8String()
    {
        return $this->getFixtureFileContent("Encode/Type/Utf8/valid.txt");
    }

    /**
     * Teste para obter conteúdo do arquivo com codificação UTF-8 inválida
     *
     * @return string
     */
    public function testGetInvalidUTF8String()
    {
        return $this->getFixtureFileContent("Encode/Type/Utf8/invalid.txt");
    }

    /**
     * Teste para obter conteúdo do arquivo com codificação ASCII válida
     *
     * @return string
     */
    public function testGetValidASCIIString()
    {
        return $this->getFixtureFileContent("Encode/Type/Ascii/valid.txt");
    }

    /**
     * Teste para a obtenção do nome da codificação da informação
     *
     * @dataProvider getEncodingNameDataProvider
     *
     * @covers Detector::getEncodingName
     */
    public function testGetEncodingName($expectedEncoding, $data)
    {
        $this->assertEquals(
            $expectedEncoding,
            Detector::getEncodingName($data)
        );
    }

    /**
     * Teste para a obtenção do nome da codificação da informação com exceção
     * de inválido
     *
     * @depends testGetInvalidUTF8String
     *
     * @expectedException Moovin\Utils\Encode\Exception\InvalidEncoding
     *
     * @covers Detector::getEncodingName
     */
    public function testGetEncodingNameException($invalidUtf8)
    {
        Detector::getEncodingName($invalidUtf8);
    }

    /**
     * Teste para obter a codificação dentro das possibilidades da informação
     *
     * @dataProvider oneOfDataProvider
     *
     * @covers Detector::oneOf
     */
    public function testOneOf($expected, array $typeList, $data)
    {
        $this->assertEquals(
            $expected,
            Detector::oneOf($typeList, $data)
        );
    }

    /**
     * Teste para obter a codificação dentro das possibilidades da informação
     * com exceção de inválido
     *
     * @depends testGetInvalidUTF8String
     *
     * @expectedException Moovin\Utils\Encode\Exception\InvalidEncoding
     *
     * @covers Detector::oneOf
     */
    public function testOneOfException($invalidUtf8)
    {
        Detector::oneOf(
            [
                Type\Ascii::class,
                Type\Utf8::class
            ],
            $invalidUtf8
        );
    }

    /**
     * Teste para a verificação se a classe implementa o contrato de tipo
     *
     * @covers Detector::classImplementsTypeContract
     */
    public function testClassImplementsTypeContract()
    {
        $this->assertTrue(
            Detector::classImplementsTypeContract(Type\Utf8::class)
        );

        $this->assertFalse(
            Detector::classImplementsTypeContract(static::class)
        );
    }

    /**
     * Provedor de informações para o testGetEncodingName()
     *
     * @return array
     */
    public function getEncodingNameDataProvider()
    {
        return [
            [
                Type\Utf8::NAME,
                $this->testGetValidUTF8String()
            ],
            [
                Type\Ascii::NAME,
                $this->testGetValidASCIIString()
            ]
        ];
    }

    /**
     * Provedor de informações para o testOneOf()
     *
     * @return array
     */
    public function oneOfDataProvider()
    {
        return [
            [
                Type\Utf8::class,
                [
                    Type\Ascii::class,
                    Type\Utf8::class
                ],
                $this->testGetValidUTF8String()
            ],
            [
                Type\Ascii::class,
                [
                    Type\Ascii::class,
                    Type\Utf8::class
                ],
                $this->testGetValidASCIIString()
            ],
            [
                Type\Utf8::class,
                [
                    Type\Utf8::class,
                    Type\Ascii::class
                ],
                $this->testGetValidASCIIString()
            ]
        ];
    }
}
