<?php

namespace Moovin\Utils\Tests\Encode\Type;

use Moovin\Utils\Tests\TestCase as BaseTestCase;
use Moovin\Utils\Encode\Type\Utf8;

/**
 * Classe de teste para a codificação do tipo UTF-8
 *
 * @package Moovin\Utils\Tests\Encode\Type
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class Utf8Test extends BaseTestCase
{
    /**
     * Obtém o conteúdo do arquivo com codificação UTF-8 válida
     *
     * @return string
     */
    public function testGetValidFileContent()
    {
        return $this->getFixtureFileContent("Encode/Type/Utf8/valid.txt");
    }

    /**
     * Obtém o conteúdo do arquivo com codificação UTF-8 inválida
     *
     * @return string
     */
    public function testGetInvalidFileContent()
    {
        return $this->getFixtureFileContent("Encode/Type/Utf8/invalid.txt");
    }

    /**
     * Obtém o conteúdo do arquivo com codificação UTF-8 inválida mas com partes
     * válidas
     *
     * @return string
     */
    public function testGetMixedFileContent()
    {
        return $this->getFixtureFileContent("Encode/Type/Utf8/mixed.txt");
    }

    /**
     * Obtém o conteúdo do arquivo com codificação LATIN-1
     *
     * @return string
     */
    public function testGetNonUTF8FileContent()
    {
        return $this->getFixtureFileContent("Encode/Type/Utf8/latin-1.txt");
    }

    /**
     * Teste para a obtenção do nome
     *
     * @covers Utf8::getName
     */
    public function testGetName()
    {
        $this->assertEquals("UTF-8", Utf8::getName());
    }

    /**
     * Teste para a detecção do tipo de codificação
     *
     * @depends testGetValidFileContent
     *
     * @depends testGetInvalidFileContent
     *
     * @depends testGetMixedFileContent
     *
     * @depends testGetNonUTF8FileContent
     *
     * @covers Utf8::detect
     */
    public function testDetect($valid, $invalid, $mixed, $nonUtf8)
    {
        $this->assertTrue(Utf8::detect($valid));

        $this->assertTrue(Utf8::detect($invalid));

        $this->assertTrue(Utf8::detect($mixed));

        $this->assertFalse(Utf8::detect($nonUtf8));
    }

    /**
     * Teste para a validação do tipo de codificação
     *
     * @depends testGetValidFileContent
     *
     * @depends testGetInvalidFileContent
     *
     * @depends testGetMixedFileContent
     *
     * @depends testGetNonUTF8FileContent
     *
     * @covers Utf8::validate
     */
    public function testValidate($valid, $invalid, $mixed, $nonUtf8)
    {
        $this->assertTrue(Utf8::validate($valid));

        $this->assertFalse(Utf8::validate($invalid));

        $this->assertFalse(Utf8::validate($mixed));

        $this->assertFalse(Utf8::validate($nonUtf8));
    }
}
