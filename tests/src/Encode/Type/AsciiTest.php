<?php

namespace Moovin\Utils\Tests\Encode\Type;

use Moovin\Utils\Tests\TestCase as BaseTestCase;
use Moovin\Utils\Encode\Type\Ascii;

/**
 * Classe de teste para a codificação do tipo ASCII
 *
 * @package Moovin\Utils\Tests\Encode\Type
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class AsciiTest extends BaseTestCase
{
    /**
     * Obtém o conteúdo do arquivo com codificação ASCII válida
     *
     * @return string
     */
    public function testGetValidFileContent()
    {
        return $this->getFixtureFileContent("Encode/Type/Ascii/valid.txt");
    }

    /**
     * Obtém o conteúdo do arquivo com codificação ASCII inválida
     *
     * @return string
     */
    public function testGetInvalidFileContent()
    {
        return $this->getFixtureFileContent("Encode/Type/Ascii/invalid.txt");
    }

    /**
     * Obtém o conteúdo do arquivo com codificação ASCII inválida mas com partes
     * válidas
     *
     * @return string
     */
    public function testGetMixedFileContent()
    {
        return $this->getFixtureFileContent("Encode/Type/Ascii/mixed.txt");
    }

    /**
     * Teste para a obtenção do nome
     *
     * @covers Ascii::getName
     */
    public function testGetName()
    {
        $this->assertEquals("ASCII", Ascii::getName());
    }

    /**
     * Teste para a detecção do tipo de codificação
     *
     * @depends testGetValidFileContent
     *
     * @depends testGetInvalidFileContent
     *
     * @depends testGetMixedFileContent
     *
     * @covers Ascii::detect
     */
    public function testDetect($valid, $invalid, $mixed)
    {
        $this->assertTrue(Ascii::detect($valid));

        $this->assertTrue(Ascii::detect($invalid));

        $this->assertTrue(Ascii::detect($mixed));
    }

    /**
     * Teste para a validação do tipo de codificação
     *
     * @depends testGetValidFileContent
     *
     * @depends testGetInvalidFileContent
     *
     * @depends testGetMixedFileContent
     *
     * @covers Ascii::validate
     */
    public function testValidate($valid, $invalid, $mixed)
    {
        $this->assertTrue(Ascii::validate($valid));

        $this->assertFalse(Ascii::validate($invalid));

        $this->assertFalse(Ascii::validate($mixed));
    }
}
