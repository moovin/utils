<?php

namespace Moovin\Utils\Tests;

use Moovin\Utils\Object;

/**
 * Teste para Moovin\Utils\Object
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class ObjectTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testa o método Object::toArray()
     *
     * @dataProvider toArrayDataProvider
     *
     * @covers Moovin\Utils\Object::toArray
     */
    public function testToArray($data, $expected)
    {
        $this->assertSame(serialize($expected), serialize(Object::toArray($data)));
    }

    /**
     * Provedor de dados para testes de conversão em array
     *
     * @return array
     */
    public function toArrayDataProvider()
    {
        return [
            [
                (object) [
                    "yolo" => "sup",
                    "sup" => (object) [
                        "mana"
                    ],
                    "mana" => (object) [
                        "yolo" => (object) [
                            "sup" => "mana"
                        ]
                    ]
                ],
                [
                    "yolo" => "sup",
                    "sup" => [
                        "mana"
                    ],
                    "mana" => [
                        "yolo" => [
                            "sup" => "mana"
                        ]
                    ]
                ]
            ]
        ];
    }
}
