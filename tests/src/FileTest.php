<?php

namespace Moovin\Utils\Tests;

use Moovin\Utils\File;

/**
 * Teste para Moovin\Utils\File
 *
 * @author Rhagni Oliveira <rhagni@moovin.com.br>
 */
class FileTest extends TestCase
{
    /**
     * Testa o método File::getExtension()
     *
     * @dataProvider getExtensionDataProvider
     *
     * @covers Moovin\Utils\File::getExtension
     */
    public function testGetExtension($file, $expected)
    {
        $this->assertEquals($expected, File::getExtension($file));
    }
    
    /**
     * Testa o método File::getMime()
     *
     * @dataProvider getMimeDataProvider
     *
     * @covers Moovin\Utils\File::getMime
     */
    public function testGetMime($file, $expected)
    {
        $this->assertEquals($expected, File::getMime($file));
    }

    /**
     * Provedor de dados para testes de obtenção de extensão dos arquivos
     *
     * @return array
     */
    public function getExtensionDataProvider()
    {
        return [
            [
                "index",
                ""
            ],
            [
                "index.html",
                "html"
            ],
            [
                "index.html.php",
                "php"
            ],
            [
                "/var/www/html/public/index.html",
                "html"
            ],
            [
                "/var/www/html/public/index",
                ""
            ],
            [
                "index.html/index.php",
                "php"
            ],
            [
                "index.html/index",
                ""
            ],
            [
                "C:\\var\\www\\index.html",
                "html"
            ],
            [
                "C:\\var\\www\\index",
                ""
            ],
            [
                "index.html\\index.php",
                "php"
            ],
            [
                "index.html\\index",
                ""
            ]
        ];
    }

    /**
     * Provedor de dados para testes de obtenção de mime dos arquivos
     *
     * @return array
     */
    public function getMimeDataProvider()
    {
        return [
            [
                "index",
                "application/octet-stream"
            ],
            [
                "index.html",
                "text/html"
            ],
            [
                "index.html.php",
                "application/octet-stream"
            ],
            [
                "/var/www/html/public/index.html",
                "text/html"
            ],
            [
                "/var/www/html/public/robots.txt",
                "text/plain"
            ]
        ];
    }
}
